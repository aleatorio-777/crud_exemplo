drop database if exists crud_exemplo;

create database crud_exemplo
character set utf8
collate utf8_unicode_ci;

create table users (
    id int unsigned not null auto_increment,
    name varchar(64) not null,
    email varchar(64) not null unique,
    password varchar(255) not null,
    primary key(id)
);

create table posts (
    id int unsigned not null auto_increment,
    user_id int unsigned not null,
    title varchar(32) not null,
    body longtext not null,
    primary key (id),
    foreign key (user_id) references user(id)
);
