# Crud de usuário

## Funcionalidades
- criar 
- ler
- atualizar
- deletar

## Recursos & endpoints

- GET /user
- GET /user/{id}
- GET /user/create
- POST /user
- GET /user/{id}/edit
- PUT|POST /user
- DELETE|GET /user/{id}/delete

## Iniciar com composer

Se você está usando linux, baixe e instale o composer globalmente com:

```bash
curl -sS https://getcomposer.org/installer -o composer-setup.php
php composer-setup.php
sudo mv composer.phar /usr/local/bin/composer
composer
```

Caso use Windows, execute o instalador em [https://getcomposer.org/Composer-Setup.exe](instalador do composer) ou siga instruções na interwebs :).

Siga os passos a seguir:

```bash
composer dump -o
```

## Uso

- Iniciar Mysql e realizar os comandos de migração presentes na pasta docs
- Iniciar o webserver embutido do php

```bash
php -S 127.0.0.1:8000 -t public
```

ou caso queira utilizar um script com o composer execute:

```bash
composer run dev
```

Acesse a url `http://127.0.0.1:8000` para obter as funcionalidades

# Next

- Adicionar funcionalidade de login com email e senha
- Adicionar funcionalidade/crud de posts para trabalhar +/- como um blog
- Se possível adicionar funcionalidade de validação no servidor
- Adicionar suporte a autoloading e composer
