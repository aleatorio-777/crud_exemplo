<?php

use CrudExemplo\Model\User;

function listAllUsers()
{
    $user = new User();

    $allUsers = $user->findAll();

    include_once dirname(__DIR__).'/View/User/list.php';
}
function createNewUser()
{
    include_once dirname(__DIR__).'/View/User/create.php';
}

function saveUser(array $data)
{
    // validar --> filter_var()

    $user = new User();
    $user->setName($data['name']);
    $user->setEmail($data['email']);
    $user->setPassword($data['password']);

    $userId = $user->save($user);

    var_dump($userId);
}
