<?php

declare(strict_types=1);

namespace CrudExemplo;

class Connection
{
    /** @var \PDO */
    private $connection;

    public function __construct()
    {
        $config = require_once __DIR__.'/../config/config.php';

        $dns = sprintf(
            'mysql:dbname=%s;host=%s;port=%s',
            $config['db']['dbname'],
            $config['db']['host'],
            $config['db']['port']
        );
        $user = $config['db']['user'];
        $password = $config['db']['password'];

        $this->connection = new \PDO($dns, $user, $password);
    }

    /**
     * @return \PDO
     */
    public function getConnection(): \PDO
    {
        return $this->connection;
    }
}
