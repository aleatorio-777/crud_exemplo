<?php

declare(strict_types=1);

namespace CrudExemplo\Model;

class User extends Model
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $email;

    /** @var string */
    private $password;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return void
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $text
     * @return bool
     */
    public function isPassword(string $text): bool
    {
        return password_verify($text, $this->password);
    }

    /**
     * @param string $text
     * @return void
     */
    public function setPassword(string $text): void
    {
        $this->password = password_hash($text, PASSWORD_BCRYPT);
    }

    public function findAll()
    {
        $sql = 'select * from users';

        try {
            $stm = $this->connection->prepare($sql);
            $stm->execute();
            $allUsers = $stm->fetchAll();
            return $allUsers;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function find(int $id)
    {
        echo 'fazer método de busca';
    }

    /**
     * @return int|bool
     */
    public function save(User $user)
    {
        $sql = "insert into users (name, email, password) values (?, ?, ?)";

        try {
        /** @var \PDOStatement */
            $stm = $this->connection->prepare($sql);

            $stm->bindValue(1, $user->getName(), \PDO::PARAM_STR);
            $stm->bindValue(2, $user->getEmail(), \PDO::PARAM_STR);
            $stm->bindValue(3, $user->getPassword(), \PDO::PARAM_STR);

            $status = $stm->execute();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return (!isset($status) || !$status)
            ? false
            : $this->connection->lastInsertId();
    }

    public function update(int $id, User $user)
    {
        echo 'Fazer update';
    }

    public function delete(int $id)
    {
        echo 'Fazer delete';
    }
}
