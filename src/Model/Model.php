<?php

declare(strict_types=1);

namespace CrudExemplo\Model;

use CrudExemplo\Connection;

abstract class Model
{
    /** @var \PDO */
    protected $connection;

    public function __construct()
    {
        $this->connection = (new Connection())->getConnection();
    }
}
