<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <title>Listar todos os usuário</title>
</head>
<body>
<main role="main" class="container">

    <a class="btn btn-lg btn-primary" href="/users/create">Criar novo usuario</a>

    <table class="table">
        <thead>
            <th>Id</th>
            <th>Nome</th>
            <th>Email</th>
            <th>Opções</th>
        </thead>

        <tbody>

            <?php foreach ($allUsers as $user) : ?>
            <tr>
                <td><?= $user['id']; ?></td>
                <td><?= $user['name']; ?></td>
                <td><?= $user['email']; ?></td>
                <td>
                    <a class="btn btn-sm btn-warning" href="<?= "http://localhost:8000/users/{$user['id']}/edit" ?>">Editar</a>
                    <a class="btn btn-sm btn-danger" href="<?= "http://localhost:8000/users/{$user['id']}/delete" ?>">Remover</a>
                </td>
            </tr>
            <?php endforeach ?>

        </tbody>

    </table>

</main>
</body>
</html>
