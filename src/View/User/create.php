<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <title>Criar novo usuário</title>
</head>
<body>
<main class="container">

    <h4 class="mb-3">Cadastro</h4>

    <form method="POST" action="/users/save">

        <div class="form-group">
            <label for="name">Nome: </label>
            <input type="text" class="form-control" id="name" placeholder="Seu nome" name="name">
        </div>

        <div class="form-group">
            <label for="email">Email: </label>
            <input type="email" class="form-control" id="email" placeholder="seuemail@example.com" name="email">
        </div>

        <div class="form-group">
            <label for="pass">Password</label>
            <input type="password" class="form-control" id="pass" placeholder="Sua senha" name="password">
        </div>

        <button class="btn btn-primary btn-lg btn-block" type="submit">Criar usuário</button>

    </form>

</main>
</body>
</html>
