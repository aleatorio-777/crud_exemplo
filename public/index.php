<?php

require_once __DIR__.'/../vendor/autoload.php';

$uri = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];

if ($uri == '/users') {
    listAllUsers();
} elseif (preg_match("/users\/\d$/", $uri, $matches)) {
    echo 'ver um usuário';
} elseif ($uri == '/users/create') {
    createNewUser();
} elseif ($method == 'POST' && $uri == '/users/save') {
    saveUser($_POST);
} elseif (preg_match("/users\/\d\/edit$/", $uri, $matches)) {
    echo 'editar usuário';
} elseif ($method == 'POST' && $uri == '/users/update') {
    // updateUser($_POST);
} elseif (preg_match("/users\/\d\/delete$/", $uri, $matches)) {
    echo 'deletar usuário';
} elseif ($uri == '/login') {
    echo 'págian de login do usuario';
} else {
    header('HTTP/1.1 404 Not Found');
    include __DIR__.'/../src/View/notfound.php';
}
